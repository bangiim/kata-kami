<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Project

Ini adalah project dari Bootcamp **Sanbercode**, project ini dikembangkan menggunakan framework Laravel versi 6.x.
<br>
Project ini dikerjakan secara team dan tiap team berisikan 3 anggota. Berikut nama-nama anggota team kami :
- Rayi Abipraya - [@Abip](https://t.me/Abip).
- Silca Silkillah Adwa - [@silcasa96](https://t.me/silcasa96).
- Muhammad Ibrahim - [@bang_iim](https://t.me/bang_iim).

## Tema Project

Pada Final Project kali ini, kami memilih tema "Forum Tanya Jawab"

## ERD System
<a href="https://ibb.co/Jky9VpQ"><img src="https://i.ibb.co/mzX3ky8/ERD-Kata-Kami.png" alt="ERD-Kata-Kami" border="0"></a>

## Link Video

Link video aplikasi : 

## Link Demo

Link demo aplikasi : http://katakami.sanbercodeapp.com/

## Installation Project
1. Buka **cmd** lalu ketikkan
2. Cloning repository : `git clone https://gitlab.com/bangiim/kata-kami.git`
3. Masuk ke directory project : `cd kata-kami`
4. `composer install`
5. Buat file `.env`, isinya diambil dari `.env.example`
6. Buat **database**, lalu masukkan nama database ke `DB_DATABASE` di file `.env`
7. `php artisan key:generate`
8. `php artisan migrate`
9. `php artisan serve`

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
