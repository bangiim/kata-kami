
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>KataKami.com | @yield('judul')</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{asset('adminlte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('adminlte/dist/css/adminlte.min.css')}}">
  {{-- CSS Custom --}}
  <style type="text/css">
    /*html, body {
      color: #636b6f;
      font-family: 'Nunito', sans-serif;
    }*/
    .btn-space{
      width: 100px;
    }
    
    .radius-card{
      border-radius: 10px;
    }

    .fixed-nav{
      position: fixed; /* Set the navbar to fixed position */
      top: 0; /* Position the navbar at the top of the page */
      width: 100%; /* Full width */
    }

    .content-top{
      margin-top: 90px;
    }
    .nav-size{
      font-size: 18px;
    }
  </style>
  {{-- Include CSS --}}
  @stack('style')
</head>
<body class="hold-transition layout-top-nav dark-mode">
<div class="wrapper">
  @include('sweetalert::alert')
  
  <!-- Navbar -->
  @include('layouts.nav')
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Theme by <a href="https://adminlte.io">AdminLTE.io</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2022 <a href="#">KataKami.Com</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{asset('adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('adminlte/dist/js/demo.js')}}"></script>
{{-- Include script --}}
@stack('script')
</body>
</html>
