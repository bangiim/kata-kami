@extends('layouts.master')

@section('judul')
Welcome
@endsection

@section('content')
  <!--
  ============================
  //      Trending Today    //
  ============================
  -->
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container content-top">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h4 class="m-0">
            <span class="rounded-circle bg-danger" style="padding:2px;">
              <i class="fas fa-fire" style="margin-left: 4px;"></i>
            </span>
            &nbsp; 
            Trending Today
          </h4>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-lg-3">
          <div class="card radius-card">
            <div class="card-body">
              <h6 class="card-title">Special title treatment</h6>

              <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-3 -->
        <div class="col-lg-3">
          <div class="card radius-card">
            <div class="card-body">
              <h6 class="card-title">Special title treatment</h6>

              <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-3 -->
        <div class="col-lg-3">
          <div class="card radius-card">
            <div class="card-body">
              <h6 class="card-title">Special title treatment</h6>

              <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-3 -->
        <div class="col-lg-3">
          <div class="card radius-card">
            <div class="card-body">
              <h6 class="card-title">Special title treatment</h6>

              <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-3 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->

  <!--
  ============================
  //    Button Settings     //
  ============================
  -->
  <!-- Main content -->
  <div class="content mt-3 mb-1">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="card radius-card">
            <div class="card-body">
              <div class="row">
                <div class="col-4">
                  <div class="btn-group btn-block">
                    <button type="button" class="btn btn-default">
                      <span class="rounded-circle bg-success" style="padding:2px;">
                        <i class="far fa-star" style="margin-left:2px; color: white;"></i>
                      </span>
                      &nbsp; Most Popular
                    </button>
                    <button type="button" class="btn btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown">
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                  </div>
                </div>
                <div class="col-4">
                  <div class="btn-group btn-block">
                    <button type="button" class="btn btn-default">
                      <span class="rounded-circle bg-primary" style="padding:2px;">
                        <i class="fas fa-sort-amount-up" style="margin-left:2px; color: white;"></i>
                      </span>
                      &nbsp; Highest Votes
                    </button>
                    <button type="button" class="btn btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown">
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                  </div>
                </div>
                <div class="col-4">
                  <div class="btn-group btn-block">
                    <button type="button" class="btn btn-default">
                      <span class="rounded-circle bg-warning" style="padding:2px;">
                        <i class="far fa-calendar-alt" style="margin-left:2px; color: white;"></i>
                      </span>
                      &nbsp; Last Thread
                    </button>
                    <button type="button" class="btn btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown">
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
        <div class="col-lg-4">
          <div class="card-body">
            <a href="#" class="btn bg-purple btn-block btn-lg">
              <i class="fas fa-pencil-alt"></i> &nbsp; Write New Question
            </a>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->

  <!--
  ============================
  //  Threads & Discussions //
  ============================
  -->
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h4 class="m-0">
            <span class="rounded-circle bg-info" style="padding:3px;">
              <i class="far fa-comments" style="margin-left: 2px;"></i>
            </span>
            &nbsp; 
            Threads & Discussions
          </h4>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="card radius-card">
            <div class="card-header">
              <h5 class="card-title m-0">Featured</h5>
            </div>
            <div class="card-body">
              <h6 class="card-title">Special title treatment</h6>

              <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>

          <div class="card radius-card">
            <div class="card-header">
              <h5 class="card-title m-0">Featured</h5>
            </div>
            <div class="card-body">
              <h6 class="card-title">Special title treatment</h6>

              <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
        <div class="col-lg-4">
          <div class="card radius-card">
            <div class="card-header">
              <h5 class="card-title m-0">
                <span class="rounded-circle bg-danger" style="padding:3px;">
                  <i class="fas fa-fire" style="margin-left: 2px;"></i>
                </span>
                &nbsp; 
                Top Trending Topic
              </h5>
            </div>
            <div class="card-body">
              <ol>
                <li>Covid-19</li>
                <li>K-Pop</li>
                <li>One Piece</li>
                <li>Perpindahan Ibu Kota</li>
              </ol>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
@endsection