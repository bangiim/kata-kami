@extends('layouts.master')

@section('judul')
    Edit Kategori {{$kategori->kategori}}
@endsection

@section('content')

<div class="content">
    <div class="container content-top">>

   
        <form action="/kategori/{{$kategori->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" value="{{$kategori->kategori}}" class="form-control" name="kategori" id="title" placeholder="Masukkan Title">
                @error('kategori')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
</div>

@endsection
