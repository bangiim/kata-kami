@extends('layouts.master')

@section('judul')
Create Kategori
@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container content-top">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Profile</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">User Profile</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="card card-info card-outline radius-card">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle"
                     src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}"
                     alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

              <p class="text-muted text-center">{{ Auth::user()->email }}</p>

              <ul class="list-group mb-3">
                <li class="list-group-item">
                  <i class="fas fa-calendar-alt mr-1"></i> Umur
                  <p class="text-muted float-right">24</p>
                </li>
                <li class="list-group-item">
                  <i class="fas fa-map-marker-alt mr-1"></i> Alamat
                  <p class="text-muted">
                    Jakarta Sealatan, Indonesia
                  </p>
                </li>
                <li class="list-group-item">
                  <i class="fas fa-book mr-1"></i> Biodata
                  <p class="text-muted">
                    B.S. in Computer Science from the University of Tennessee at Knoxville
                  </p>
                </li>
              </ul>
              <a href="/profile/create" class="btn btn-info btn-block"><i class="fas fa-pencil-alt" style="margin-right: 4px;"></i> Edit Profile</a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <div class="card radius-card">
            <div class="card-header">
              <h5 class="card-title m-1">
                <span class="rounded-circle bg-danger" style="padding:5px;">
                  <i class="fas fa-list-ul" style="margin-left: 2px;"></i>
                </span>
                &nbsp;
                Kategori Saya
              </h5>
            </div>
            <div class="card-body">
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">Covid-19</li>
                <li class="list-group-item">K-Pop</li>
                <li class="list-group-item">One Piece</li>
                <li class="list-group-item">Perpindahan Ibu Kota</li>
              </ul>
            </div>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <!-- Main content -->
          <div class="content mb-2">
            <div class="container">
              <div class="row">
                <div class="col-lg-7">
                  <div class="card radius-card">
                    <div class="card-body">
                      <!-- Start kode untuk form pencarian -->
                      <form class="form" method="get" action="/search">
                        <div class="input-group">
                          <input class="form-control" type="search" placeholder="Search ..." aria-label="Search" name="keyword">
                          <div class="input-group-append">
                            <button class="btn btn-info" type="submit">
                              <i class="fas fa-search fa-fw"></i>
                            </button>
                            <button class="btn btn-warning" type="button" onclick="location.href='/';">
                              <i class="fas fa-times fa-fw"></i>
                            </button>
                          </div>
                        </div>
                      </form>
                      <!-- Start kode untuk form pencarian -->
                      @if ($message = Session::get('success'))
                      <div class="alert alert-success">
                          <p>{{ $message }}</p>
                      </div>
                      @endif
                    </div>
                  </div>
                </div>
                <!-- /.col-md-6 -->
                <div class="col-lg-5">
                  <div class="card-body">
                    <a href="/pertanyaan/create" class="btn bg-purple btn-block btn-lg">
                      <i class="fas fa-pencil-alt"></i> &nbsp; Write New Question
                    </a>
                  </div>
                </div>
                <!-- /.col-md-6 -->
              </div>
              <!-- /.row -->
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content -->
          <div class="card  radius-card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link rounded-pill" href="/profile">Activity</a></li>
                <li class="nav-item"><a class="nav-link active rounded-pill" href="kategori">Category</a></li>
              </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
                <div class="card radius-card mt-1">
                    <div class="card-body">
                        <form class="form-horizontal mt-2" method="POST" action="/kategori">
                            @csrf
                            <div class="form-group row">
                              <label for="inputName" class="col-sm-2 col-form-label">Add kategori</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control @error('kategori') is-invalid @enderror" placeholder="Kategori" name="kategori" value="">

                                @error('kategori')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                                @enderror
                              </div>
                            </div>
                            
                            <div class="form-group row">
                              <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-danger">Submit</button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@endsection

@push('style')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endpush

@push('script')
  <!-- DataTables -->
  <script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#example").DataTable();
    });
  </script>
@endpush