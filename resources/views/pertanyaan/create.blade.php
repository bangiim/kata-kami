@extends('layouts.master')

@section('judul')
Question
@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container content-top">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Question</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/profile">Profile</a></li>
            <li class="breadcrumb-item active">Ask Question</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <div class="card radius-card">
            <div class="card-header">
              <h5 class="card-title m-1">
                <span class="rounded-circle bg-danger" style="padding:5px;">
                  <i class="fas fa-list-ul" style="margin-left: 2px;"></i>
                </span>
                &nbsp; 
                Kategori Saya
              </h5>
            </div>
            <div class="card-body">
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">Covid-19</li>
                <li class="list-group-item">K-Pop</li>
                <li class="list-group-item">One Piece</li>
                <li class="list-group-item">Perpindahan Ibu Kota</li>
              </ul>
            </div>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <!-- Main content -->
          <div class="card  radius-card">
            <div class="card-header">
              <h4>Ask Question</h4>
            </div><!-- /.card-header -->
            <div class="card-body">
                <form action="/pertanyaan" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="title">Pertanyaan</label>
                        <textarea class="form-control" id="summernote" name="pertanyaan" placeholder="Tuliskan pertanyaan yang ingin kamu tanyakan disini"></textarea>
                        @error('pertanyaan')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="exampleInputFile">Gambar</label>
                                <div class="input-group">
                                  <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">Input gambar jika diperlukan</label>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="title">Kategori</label>
                                <select name="kategori_id" class="form-control select2bs4 bg-dark">
                                    @foreach ($kategori as $item)
                                        <option selected>Select a Category</option>
                                        <option value="{{$item->id}}">{{$item->kategori}}</option>
                                    @endforeach
                                </select>
                                @error('kategori_id')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    
                    <input type="hidden" value="{{Auth::user()->id}}" name="user_id">
                    
                    <button type="submit" class="btn btn-primary mt-3">Tambah</button>
                </form>
            </div><!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@endsection

@push('style')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/summernote/summernote-bs4.min.css')}}">
@endpush

@push('script')
    <!-- Select2 -->
    <script src="{{asset('adminlte/plugins/select2/js/select2.full.min.js')}}"></script>
    <!-- Summernote -->
    <script src="{{asset('adminlte/plugins/summernote/summernote-bs4.min.js')}}"></script>
    <!-- Page specific script -->
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2bs4').select2({
              theme: 'bootstrap4'
            });

            // Summernote
            $('#summernote').summernote({
                height: 200
            });
        });
    </script>
@endpush