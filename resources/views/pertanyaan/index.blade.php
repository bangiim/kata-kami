@extends('layouts.master')

@section('judul')
Welcome
@endsection

@section('content')
<div class="content">
    <div class="container content-top">
        <a href="/pertanyaan/create" class="btn btn-success"><i class="fas fa-plus"></i>Tambah Pertanyaan</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">no</th>
        <th scope="col">pertanyaan</th>
        <th scope="col">gambar</th>
        <th scope="col">kategori</th>
        <th scope="col">action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($pertanyaan as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->pertanyaan}}</td>
                <td>{{$item->gambar}}</td>
                <td>{{$item->kategori->kategori}}</td>
                <td>

                    <form action="/pertanyaan/{{$item->id}}" method="post">
                    <a href="/pertanyaan/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>

                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-primary btn-sm" value="Delete">

                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3">Data masih kosong</td>
            </tr>
        @endforelse
      
    </tbody>
  </table>
    </div>
</div>

@endsection