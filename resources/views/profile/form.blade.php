@extends('layouts.master')

@section('judul')
Edit Profile
@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container content-top">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Profile</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/profile">Home</a></li>
            <li class="breadcrumb-item active">Edit Profile</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content mb-3">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="card card-info card-outline radius-card">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle"
                     src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}"
                     alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

              <p class="text-muted text-center">{{ Auth::user()->email }}</p>

              <ul class="list-group mb-3">
                <li class="list-group-item">
                  <i class="fas fa-calendar-alt mr-1"></i> Umur
                  <p class="text-muted float-right">24</p>
                </li>
                <li class="list-group-item">
                  <i class="fas fa-map-marker-alt mr-1"></i> Alamat 
                  <p class="text-muted">
                    Jakarta Sealatan, Indonesia
                  </p>
                </li>
                <li class="list-group-item">
                  <i class="fas fa-book mr-1"></i> Biodata
                  <p class="text-muted">
                    B.S. in Computer Science from the University of Tennessee at Knoxville
                  </p>
                </li>
              </ul>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <!-- Main content -->
          <div class="card card-info card-outline radius-card">
            <div class="card-header">
              <h4>Settings</h4>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              {{-- Settings --}}
              <div class="active tab-pane" id="settings">
                <form class="form-horizontal" method="POST" action="/profile" >
                  @csrf
                  <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                  <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Name" value="{{ Auth::user()->name }}">
                    </div>
                    
                    @error('name')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                    @enderror
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" value="{{ Auth::user()->email }}" readonly>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputName2" class="col-sm-2 col-form-label">Umur</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control @error('umur') is-invalid @enderror" name="umur" placeholder="Umur">

                      @error('umur')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputExperience" class="col-sm-2 col-form-label">Biodata</label>
                    <div class="col-sm-10">
                      <textarea class="form-control @error('biodata') is-invalid @enderror" name="biodata" placeholder="Biodata"></textarea>

                       @error('biodata')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputExperience" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                      <textarea class="form-control @error('alamat') is-invalid @enderror" name="alamat" placeholder="Alamat"></textarea>

                      @error('alamat')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Save</button>
                      {{-- <a href="/profile" class="btn btn-danger">Save</a> --}}
                    </div>
                  </div>
                </form>
              </div>
            </div><!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@endsection
