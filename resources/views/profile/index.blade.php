@extends('layouts.master')

@section('judul')
Profile
@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container content-top">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Profile</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">User Profile</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="card card-info card-outline radius-card">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle"
                     src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}"
                     alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

              <p class="text-muted text-center">{{ Auth::user()->email }}</p>

              <ul class="list-group mb-3">
                <li class="list-group-item">
                  <i class="fas fa-calendar-alt mr-1"></i> Umur
                  <p class="text-muted float-right">24</p>
                </li>
                <li class="list-group-item">
                  <i class="fas fa-map-marker-alt mr-1"></i> Alamat
                  <p class="text-muted">
                    Jakarta Sealatan, Indonesia
                  </p>
                </li>
                <li class="list-group-item">
                  <i class="fas fa-book mr-1"></i> Biodata
                  <p class="text-muted">
                    B.S. in Computer Science from the University of Tennessee at Knoxville
                  </p>
                </li>
              </ul>
              <a href="/profile/create" class="btn btn-info btn-block"><i class="fas fa-pencil-alt" style="margin-right: 4px;"></i> Edit Profile</a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <div class="card radius-card">
            <div class="card-header">
              <h5 class="card-title m-1">
                <span class="rounded-circle bg-danger" style="padding:5px;">
                  <i class="fas fa-list-ul" style="margin-left: 2px;"></i>
                </span>
                &nbsp;
                Kategori Saya
              </h5>
            </div>
            <div class="card-body">
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">Covid-19</li>
                <li class="list-group-item">K-Pop</li>
                <li class="list-group-item">One Piece</li>
                <li class="list-group-item">Perpindahan Ibu Kota</li>
              </ul>
            </div>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <!-- Main content -->
          <div class="content mb-2">
            <div class="container">
              <div class="row">
                <div class="col-lg-7">
                  <div class="card radius-card">
                    <div class="card-body">
                      <!-- Start kode untuk form pencarian -->
                      <form class="form" method="get" action="/search">
                        <div class="input-group">
                          <input class="form-control" type="search" placeholder="Search ..." aria-label="Search" name="keyword">
                          <div class="input-group-append">
                            <button class="btn btn-info" type="submit">
                              <i class="fas fa-search fa-fw"></i>
                            </button>
                            <button class="btn btn-warning" type="button" onclick="location.href='/';">
                              <i class="fas fa-times fa-fw"></i>
                            </button>
                          </div>
                        </div>
                      </form>
                      <!-- Start kode untuk form pencarian -->
                      @if ($message = Session::get('success'))
                      <div class="alert alert-success">
                          <p>{{ $message }}</p>
                      </div>
                      @endif
                    </div>
                  </div>
                </div>
                <!-- /.col-md-6 -->
                <div class="col-lg-5">
                  <div class="card-body">
                    <a href="/pertanyaan/create" class="btn bg-purple btn-block btn-lg">
                      <i class="fas fa-pencil-alt"></i> &nbsp; Write New Question
                    </a>
                  </div>
                </div>
                <!-- /.col-md-6 -->
              </div>
              <!-- /.row -->
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content -->
          <div class="card  radius-card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active rounded-pill" href="/profile">Activity</a></li>
                <li class="nav-item"><a class="nav-link rounded-pill" href="kategori">Category</a></li>
              </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
              <!-- Post -->
              <div class="post">
                <div class="user-block">
                  <img class="img-circle img-bordered-sm" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="user image">
                  <span class="username">
                    <a href="#">Jonathan Burke Jr.</a>
                    <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                  </span>
                  <span class="description">Shared publicly - 7:30 PM today</span>
                </div>
                <!-- /.user-block -->
                <p>
                  Lorem ipsum represents a long-held tradition for designers,
                  typographers and the like. Some people hate it and argue for
                  its demise, but others ignore the hate as they create awesome
                  tools to help create filler text for everyone from bacon lovers
                  to Charlie Sheen fans.
                </p>

                <p>
                  <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                  <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                  <span class="float-right">
                    <a href="#" class="link-black text-sm">
                      <i class="far fa-comments mr-1"></i> Comments (5)
                    </a>
                  </span>
                </p>

                <input class="form-control form-control-sm" type="text" placeholder="Type a comment">
              </div>
              <!-- /.post -->

              <!-- Post -->
              <div class="post clearfix">
                <div class="user-block">
                  <img class="img-circle img-bordered-sm" src="{{asset('adminlte/dist/img/user7-128x128.jpg')}}" alt="User Image">
                  <span class="username">
                    <a href="#">Sarah Ross</a>
                    <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                  </span>
                  <span class="description">Sent you a message - 3 days ago</span>
                </div>
                <!-- /.user-block -->
                <p>
                  Lorem ipsum represents a long-held tradition for designers,
                  typographers and the like. Some people hate it and argue for
                  its demise, but others ignore the hate as they create awesome
                  tools to help create filler text for everyone from bacon lovers
                  to Charlie Sheen fans.
                </p>

                <form class="form-horizontal">
                  <div class="input-group input-group-sm mb-0">
                    <input class="form-control form-control-sm" placeholder="Response">
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-danger">Send</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.post -->

              <!-- Post -->
              <div class="post">
                <div class="user-block">
                  <img class="img-circle img-bordered-sm" src="{{asset('adminlte/dist/img/user6-128x128.jpg')}}" alt="User Image">
                  <span class="username">
                    <a href="#">Adam Jones</a>
                    <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                  </span>
                  <span class="description">Posted 5 photos - 5 days ago</span>
                </div>
                <!-- /.user-block -->
                <div class="row mb-3">
                  <div class="col-sm-6">
                    <img class="img-fluid" src="{{asset('adminlte/dist/img/photo1.png')}}" alt="Photo">
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-6">
                    <div class="row">
                      <div class="col-sm-6">
                        <img class="img-fluid mb-3" src="{{asset('adminlte/dist/img/photo2.png')}}" alt="Photo">
                        <img class="img-fluid" src="{{asset('adminlte/dist/img/photo3.jpg')}}" alt="Photo">
                      </div>
                      <!-- /.col -->
                      <div class="col-sm-6">
                        <img class="img-fluid mb-3" src="{{asset('adminlte/dist/img/photo4.jpg')}}" alt="Photo">
                        <img class="img-fluid" src="{{asset('adminlte/dist/img/photo1.png')}}" alt="Photo">
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->

                <p>
                  <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                  <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                  <span class="float-right">
                    <a href="#" class="link-black text-sm">
                      <i class="far fa-comments mr-1"></i> Comments (5)
                    </a>
                  </span>
                </p>

                <input class="form-control form-control-sm" type="text" placeholder="Type a comment">
              </div>
              <!-- /.post -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@endsection
