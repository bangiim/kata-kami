<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = 'pertanyaan';
    protected $fillable = ['pertanyaan', 'gambar', 'user_id', 'kategori_id'];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }

}

    
